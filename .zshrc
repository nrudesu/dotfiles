path+=('/home/suki/bin')
export PATH
export DRI_PRIME=1
source ~/.antigen.zsh
antigen init ~/.antigenrc

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/suki/Downloads/google-cloud-sdk/path.zsh.inc' ]; then . '/home/suki/Downloads/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/suki/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/suki/Downloads/google-cloud-sdk/completion.zsh.inc'; fi
