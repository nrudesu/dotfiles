# dotfile
My current dotfiles

## Dependency 

- i3
- i3status
- redshift (manual edit for your location & configuration)
- urxvt
- feh
- Random folder (In this dotfiles : /home/suki/Menma\_dump)
- Arc-Dark ( for apps using gtk)
- Devilspie

## Screenshots

![urxvt](/pic/1.png)
![telegram](pic/2.png)
![firefox](/pic/3.png)
![anki](/pic/4.png)
